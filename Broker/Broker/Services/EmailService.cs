using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using System;

namespace Broker.Services
{
    public class EmailService
    {
        public async Task SendEmailAsync(string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Он-лайн Недвижимость", "buyandlive@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("", "e.v.glushkov605@gmail.com"));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.ru", 465);
                await client.AuthenticateAsync("buyandlive@yandex.ru", "Buyandlive!");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}
