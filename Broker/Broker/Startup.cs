﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Broker
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(name: "malevich", template: "Malevich", defaults: new { controller = "Home", action = "Malevich" });
                routes.MapRoute(name: "novatorov", template: "Novatorov", defaults: new { controller = "Home", action = "Novatorov" });
                routes.MapRoute(name: "suhodolsky", template: "Suhodolsky", defaults: new { controller = "Home", action = "Suhodolsky" });
                routes.MapRoute(name: "shishim", template: "Shishim", defaults: new { controller = "Home", action = "Shishim" });
                routes.MapRoute(name: "solnechny", template: "Solnechny", defaults: new { controller = "Home", action = "Solnechny" });
                routes.MapRoute(name: "svetly", template: "Svetly", defaults: new { controller = "Home", action = "Svetly" });
                routes.MapRoute(name: "priv", template: "Priv", defaults: new { controller = "Home", action = "Privacy" });
            });
        }
    }
}
