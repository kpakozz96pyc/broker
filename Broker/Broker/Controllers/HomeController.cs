﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using Broker.Services;


namespace Broker.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Malevich()
        {
            return View("~/Views/Malevich/Index.cshtml");
        }

        public IActionResult Novatorov()
        {
            return View("~/Views/Novatorov/Index.cshtml");
        }

        public IActionResult Suhodolsky()
        {
            return View("~/Views/Suhodolsky/Index.cshtml");
        }

        public IActionResult Shishim()
        {
            return View("~/Views/Shishim/Index.cshtml");
        }

        public IActionResult Solnechny()
        {
            return View("~/Views/Solnechny/Index.cshtml");
        }

        public IActionResult Svetly()
        {
            return View("~/Views/Svetly/Index.cshtml");
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SaveCall(string name, string phone)
        {
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(phone))
            {
                var s = new EmailService();
                await s.SendEmailAsync("Заявка на звонок", "Имя: " + name + Environment.NewLine + "Телефон: " + phone);
            }

            return Ok(null);
        }
    }
}
